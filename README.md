# AMS Flight Controller

This repository contains firmware and parameters used for the flight controller of the drone.

## Requirements

Note: The installation of the build setup is described for OS X.  
To build the firmware, a supported version of the ARM embedded toolchain is needed.

### GNU Embedded Toolchain for Arm

Download and unpack [GNU Arm Embedded Toolchain Version 6-2017-q2-update](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads/6-2017-q2-update) to `/opt` folder.

Add the following to the `.bash_profile`

    # Path for ROSflight Firmware
    export PATH=$PATH:/opt/gcc-arm-none-eabi-6-2017-q2-update/bin

Reload `.bash_profile` in terminal with

    source ~/.bash_profile

Test installation with

    arm-none-eabi-gcc --version

## Installation

Clone the firmware code and init all submodules

    git clone https://github.com/rosflight/firmware
    cd firmware
    git submodule update --init --recursive

Build the firmware from source

    # make all
    make

    # F4 Boards only
    make BOARD=REVO

    # F1 Boards only
    make BOARD=NAZE

## How to use

Flash the firmware to the Flight Controller. If this doesn't work, follow the instructions [here](https://www.dronetrest.com/t/how-to-update-firmware-on-the-flip32-naze32-flight-controller/646).

### Flash the Firmware

#### F4 Boards

Flash the firmware to the board by running

    make BOARD=REVO flash

If necessary, specify the serial port with

    make BOARD=REVO SERIAL_DEVICE=/dev/ttyACM0 flash

#### F1 Boards

Flash the firmware to the board by running

    make BOARD=NAZE flash

If necessary, specify the serial port with

     make BOARD=REVO SERIAL_DEVICE=/dev/ttyUSB0 flash

### Load Parameters from file

Parameters are saved in YAML format. If parameters should be reloaded from file (replace `parameters` with `Flip32` or `Naze32`).

    rosservice call /param_load_from_file ~/ams-fc/parameters/{parameters}.yml

### Write Parameters

After loading parameters from file, they need to be written to the Flight Controller to persist after reboot.

    rosservice call /param_write
